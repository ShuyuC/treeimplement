package Demo;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class Tree {

	private int maxDepth = 0;
	private Node root = null;

	Tree() {
		this.root = null;
		this.init();
	};
	Tree(Node n) {
		this.root = n;
		this.init();
	};
	
	private void init() {
		calcMaxDepth(this.root, 0);
		this.maxDepth = this.maxDepth + 1;
	}
	
	public Node getRoot() {
		return root;
	}

	public int getDepth() {
		return this.maxDepth;
	}

	private void printBlank(int count) {
		for (int i = 0; i < count; i++)
			System.out.print(" ");
	}
	private void printBlankAfter(int count) {
		for (int i = 0; i < count; i++)
			System.out.print(" ");
	}
	private void printNodeInLevel(List<Node> nodesInLevel, int level) {
		if (nodesInLevel.isEmpty() || level >= maxDepth)
			return;

		List<Node> nodeInNextLevel = new ArrayList<Node>();
		int  nodeUnit = 3;
		int  blank = (int) Math.pow(2, (int) maxDepth - 1 - level)*nodeUnit;
		int  blankIndex = 0;
		
		//print tree branch
		for (Node node : nodesInLevel) {
			printBlank(blank);
			
			if (node != null) {
				if (node.isLeftChild()) {
					System.out.format("  /");
				}
				if (node.isRightChild()) {
					System.out.format(" \\ ");
				}

			} else {
				printBlank(nodeUnit);

			}
			printBlank(blank-nodeUnit);
		}
		System.out.println("");

		//print tree data
		for (Node node : nodesInLevel) {
			
			printBlank(blank);
			
			if (node != null) {
				System.out.format("%3d", node.getData());
				if (node.hasLeftChild()) {
					nodeInNextLevel.add(node.getLeftChild());
					node.getLeftChild().addParent(node);
				} else {
					nodeInNextLevel.add(null);
				}
				if (node.hasRightChild()) {
					nodeInNextLevel.add(node.getRightChild());
					node.getRightChild().addParent(node);
				} else {
					nodeInNextLevel.add(null);
				}
			} else {
				nodeInNextLevel.add(null);
				nodeInNextLevel.add(null);
				printBlank(nodeUnit);
			}
			printBlank(blank - nodeUnit);
		}
		System.out.println("");

		printNodeInLevel(nodeInNextLevel, level + 1);

	}

	public void printTree() {
		List<Node> nodeInNextLevel = new ArrayList<Node>();
		nodeInNextLevel.add(this.root);
		printNodeInLevel(nodeInNextLevel, 0);
	}

	// calc MaxDepth with dfs
	private void calcMaxDepth(Node n, int level) {
		if (n == null) {
			return;
		}
		//System.out.format("%d(%d)\n", n.getData(), maxDepth);
		if (level > this.maxDepth) {
			maxDepth = level;
		}
		if (n.hasLeftChild()) {
			calcMaxDepth(n.getLeftChild(), level + 1);
		}
		if (n.hasRightChild()) {
			calcMaxDepth(n.getRightChild(), level + 1);
		}
	}
	
	// run user input with bfs
	public void userInput() {
		int index = 1;
		int nodeNum = 1;

	    LinkedList<Node> queue = new LinkedList<Node>();

	    Node rootNode = new Node();
	    this.root = rootNode;
	    queue.add(rootNode);

	    index = 1;
	    while(queue.size()!=0) {
	    	
	    	Node outNode = null;
	    	outNode = queue.poll();
	    	
	    	Scanner scan = new Scanner(System.in);
	    	if(index != 1) {
	    		System.out.format("請你輸入第%d個元素的值?", index);
	    	}else {
	    		System.out.format("請你輸入root的值?");
	    	}
			int data = scan.nextInt();
			outNode.setData(data);
			//nodeList.add(outNode);
			
			if(index != 1) {
				System.out.format("請問第%d個元素有左兒子嗎?(有:1,無:0)", index);
			}else {
				System.out.format("請問第%d個元素(root)有左兒子嗎?(有:1,無:0)", index);
			}
			
			int hasLeftChild = scan.nextInt();
	    	if(hasLeftChild == 1) {
	    		outNode.addLeftChild(new Node());
	    		queue.add(outNode.getLeftChild());
	    		nodeNum++;                                                                                                                              
	    	}
	    	if(index != 1) {
	    		System.out.format("請問第%d個元素有右兒子嗎?(有:1,無:0)", index);
	    	}else {
	    		System.out.format("請問第%d個元素(root)有右兒子嗎?(有:1,無:0)", index);	
	    	}
	    	int hasRightLeftChild = scan.nextInt();
	    	if(hasRightLeftChild == 1) {
	    		outNode.addRightChild(new Node());
	    		queue.add(outNode.getRightChild());
	    		nodeNum++;
	    	}
	    	index++;
	    }
	    //System.out.println("end");
	 // return nodeList.get(0);
	    this.init();

	}
}
