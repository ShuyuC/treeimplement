package Demo;

public class Node {
	private int data;
	private Node left;
	private Node right;
	private Node parent;
	private int level;
	
	Node (int d){
		this.data = d;
		this.left = null;
		this.right = null;
		this.parent = null;
	}
	Node (){
		this.data = 0;
		this.left = null;
		this.right = null;
	}

	public void setData(int data) {
		this.data = data;
	};
	public int getData() {
		return this.data;
	};
	
	public void addParent(Node n) {
		this.parent = n;
	};
	public void addLeftChild(Node n) {
			this.left = n;
	};
	public void addRightChild(Node n) {
			this.right = n;
	};

	public boolean hasLeftChild() {
		if(this.left!=null)
			return true;
		else
			return false;
	};
	public boolean hasRightChild() {
		if(this.right!=null)
			return true;
		else
			return false;
	};
	public boolean hasParent() {
		if(this.parent!=null)
			return true;
		else
			return false;
	};
	
	public Node getRightChild() {
		return this.right;
	};
	public Node getLeftChild() {
		return this.left;
	};
	public Node getParent() {
		return this.parent;
	};

	public boolean isLeftChild() {
		if (this != null) {
			if (this.getParent() != null) {
				if (this.getParent().hasLeftChild()) {
					if (this.getParent().getLeftChild().equals(this)) {
						return true;
					}
				}
			}
		}
		return false;
	};
	public boolean isRightChild() {
		if (this != null) {
			if (this.getParent() != null) {
				if (this.getParent().hasRightChild()) {
					if (this.getParent().getRightChild().equals(this)) {
						return true;
					}
				}
			}
		}
		return false;
	};
	
}
