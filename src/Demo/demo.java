package Demo;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Scanner;

public class demo {
	public static void main(String args[]) throws IOException {
		
		//test3();
		
		System.out.println("test1 :");
		test1();
		System.out.println("test2 : ");
		test2();
		
	}
	
	private static void test3() throws IOException {
		Tree t = new Tree();
		t.userInput();
		t.printTree();
	}
	private static void test1() {
		Node n1 = new Node(1);
		Node n2 = new Node(2);
		Node n3 = new Node(3);
		Node n4 = new Node(4);
		Node n5 = new Node(5);
		Node n6 = new Node(6);
		Node n7 = new Node(7);
		
		n1.addLeftChild(n2);
		n1.addRightChild(n3);
		
		n2.addLeftChild(null);
		n2.addRightChild(n4);
		
		n3.addLeftChild(n5);
		n3.addRightChild(null);
		
		n4.addLeftChild(n6);
		n4.addRightChild(null);
		
		n5.addLeftChild(null);
		n5.addRightChild(n7);
		
		n6.addLeftChild(null);
		n6.addRightChild(null);
		
		n7.addLeftChild(null);
		n7.addRightChild(null);
		
		Tree t = new Tree(n1);
		t.getDepth();
		t.printTree();
	}
	private static void test2() {
		Node n1 = new Node(1);
		Node n2 = new Node(2);
		Node n3 = new Node(3);
		Node n4 = new Node(4);
		Node n5 = new Node(5);
		Node n6 = new Node(6);
		Node n7 = new Node(7);
		
		n1.addLeftChild(n2);
		n1.addRightChild(n3);
		
		n2.addLeftChild(n4);
		n2.addRightChild(null);
		
		n3.addLeftChild(null);
		n3.addRightChild(n5);
		
		n4.addLeftChild(null);
		n4.addRightChild(n6);
		
		n5.addLeftChild(n7);
		n5.addRightChild(null);
		
		n6.addLeftChild(null);
		n6.addRightChild(null);
		
		n7.addLeftChild(null);
		n7.addRightChild(null);
		
		Tree t = new Tree(n1);
		t.getDepth();
		t.printTree();
	}

}
